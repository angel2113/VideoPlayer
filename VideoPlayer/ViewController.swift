//
//  ViewController.swift
//  VideoPlayer
//
//  Created by Angel Callejas on 08/08/18.
//  Copyright © 2018 Angel Callejas. All rights reserved.
//

import UIKit
import WebKit;

class ViewController: UIViewController {

    @IBOutlet weak var container: WKWebView!
    @IBOutlet weak var OpenBtn: UIButton!
    @IBOutlet weak var InputText: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func openVideo(_ sender: Any) {
        let auxUrl = InputText.text
        let url:[String]? = auxUrl?.components(separatedBy: "=")
        if(url?.count == 1){
            setAlert()
        } else{
            let url = URL(string: auxUrl!)
            let req = URLRequest(url: url!)
            container.load(req)
        }
        
    }
    
    func setAlert(){
        let alert = UIAlertController(title: "Url error", message: "You should use a url format https://www.youtube.com/watch?v=ggRIlXGOe1k", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        self.present(alert, animated: true)
    }
    
}

